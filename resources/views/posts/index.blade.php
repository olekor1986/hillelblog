@extends('templates.basic')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">{{ $pageTitle }}</h1>
            <p>Read with pleasure!</p>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        {{-- $posts --}}
        @foreach($posts as $post)
            <div class="col-md-4">
                <h2>{{$post->title}}</h2>
                <p><strong>{{$post->user->name}}</strong></p>
                <p>{{$post->intro}} </p>
                <p><a class="btn btn-secondary" href="/posts/{{$post->slug}}" role="button">View details &raquo;</a></p>
                <p><a class="btn btn-primary" href="/posts/{{$post->slug}}/edit" role="button">Edit post</a></p>
                <form action="/posts/{{$post->slug}}" method="post">
                    @method('delete')
                    @csrf
                    <button class="btn btn-danger">Delete</button>
                </form>
            </div>
        @endforeach
    </div>
    <hr>
@endsection
