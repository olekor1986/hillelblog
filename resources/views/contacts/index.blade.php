@extends('templates.basic')
@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Contacts page</h1>
            <p>Contact me!</p>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>My contacts</h2>
            <ul>
                <li>Email: john@gmail.com</li>
                <li>Phone: +38093-111-11-11</li>
            </ul>

        </div>
    </div>
    <hr>
@endsection
