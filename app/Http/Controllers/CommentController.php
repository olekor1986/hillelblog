<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\CommentsRequest;
use App\Http\Requests\CommentsUpdateRequest;

class CommentController extends Controller
{


     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentsRequest $request)
    {
        Comment::create($request->all());
        return back()->with(['status' => 'Comment successfully created!']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        return view('comments.edit', compact('comment'));
    }


    public function update(CommentsUpdateRequest $request, Comment $comment)
    {
        $comment->update($request->all());
        return redirect('/posts/'.$comment->post->slug)
            ->with(['status' => 'Comment successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return back()->with(['status' => 'Comment successfully deleted!']);
    }
}
